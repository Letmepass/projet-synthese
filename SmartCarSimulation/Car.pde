class Car {

  //--====--TRAIT--====--//

  final String[] inputText = {
    "Vision Gauche:", 
    "Vision Centre:", 
    "Vision Droite:", 
    "Angle de Rotation:", 
    "Vitesse:", 
    "Acceleration:", 
    "Avance:", 
    "Droite:", 
    "Gauche:", 
    "Break:"
  };
  final static float MAXSPEED = 0.2;
  final static float MAXDIR = 0.04;
  PVector start;
  //Nombre de inputs//
  final int nombreInputs = 10;
  //Nombre de inputs//0
  final int nombreOutputs = 6;
  NeuralNetwork brain;

  float fitness=0;

  String name;

  color col;
  PVector position;
  PVector acceleration;
  int size = 5;
  float vision = 10;
  float eyeDeviation=0.25*PI;
  float directionChangeRate=0;
  float speed=0;
  double[] inputs = new double[nombreInputs];
  double[] outputs=null;
  boolean dead = false;
  //--====--Directions--====--//

  PVector directionL;
  PVector directionC;
  PVector directionR;

  PVector DL;
  PVector DC;
  PVector DR;

  float inputR=1;
  float inputL=1;
  float inputC=1;


  //--====--Constructeur--====--// 
  Car(PVector start) {
  
    this.start=start;
    outputs=null;
    acceleration= new PVector();
    col = color(random(255), random(255), random(255));
    name = Name.give(int(random(Name.max)));
    brain = new NeuralNetwork(nombreInputs, nombreOutputs);
    position = start.copy();
    DC = PVector.random2D();
    DC = new PVector(1, 0);
  }
  //--====--Reset--====--//
  void reset() {
    speed=0;
    directionChangeRate=0;
    fitness=0;
    acceleration= new PVector();
    position = start.copy();
    DC = PVector.random2D();
    DC = new PVector(1, 0);

    dead = false;
    update();
    inputR=1;
    inputL=1;
    inputC=1;
  }
  //--====--Update--====--//
  void update() {
    DC.setMag(size+5);
    DL = DC.copy();
    DR = DC.copy();
    DL.rotate(eyeDeviation); 
    DR.rotate(-eyeDeviation);
    directionR = PVector.add(DR.copy().mult(vision), position);
    directionL = PVector.add(DL.copy().mult(vision), position);
    directionC = PVector.add(DC.copy().mult(vision), position);
  }
  //--====--Show--====--//
  void show() {
    if (!dead&&(PVector.sub(position, start)).mag()>radius) {
      pushMatrix();
      translate(position.x, position.y); 
      rotate(DC.copy().heading()+PI/2); 
      image(car, 0, 0);
      popMatrix();
      fill(col);
      ellipse(position.x, position.y, size, size);
      rectMode(CENTER);
      textSize(25);
      if (PVector.sub(position, new PVector(mouseX, mouseY)).mag()<15) {
        text(name, position.x, position.y-150);
        text("Fitness : "+fitness, position.x, position.y-100);
        textSize(15);
        fill(50, 50, 50, 20);
        strokeWeight(5);
        stroke(col);
        rect(position.x, position.y, 50, 50);
        strokeWeight(1);
        fill(col);
        for (int i =0; i<nombreInputs; i++) {
          text(inputText[i]+" "+((float)inputs[i])+"", position.x+50, position.y+i*15-50);
        }
        noStroke();
        if (keyPressed) {
          if (key=='d') {
            fitness-=1000;
          }
          if (key=='a') {
            fitness+=1000;
          }
        }
      }
      rectMode(0);
    } else {
      pushMatrix();
      translate(position.x, position.y); 
      rotate(DC.copy().heading()+PI/2); 
      tint(255, 5);
      image(car, 0, 0);
      tint(255, 255);
      popMatrix();
      fill(col, 5);
      ellipse(position.x, position.y, size, size);
    }
  }
  //--====--Run--====--//
  void run(PVector direction) {
    acceleration.add(direction.mult(speed));
    position.add(acceleration);
  }

  //--====--Life--====--//
  void life() {
    update();
    show();
    calculeInputs();
    outputs = brain.fire(inputs);
    int decision=0;
    double max=0;
    for (int i=0; i<4; i++) {
      if (outputs[i]>max) {
        decision=i;
        max=outputs[i];
      }
    }
    outputs[0]=0;
    outputs[1]=0;
    outputs[2]=0;
    outputs[3]=0;
    outputs[decision]=1;
    directionChangeRate=(float)(outputs[4]*MAXDIR*PI);
    speed=(float)(outputs[5]*MAXSPEED);
    choseAction(decision);
    calculeFitness();
  }
  //--====--calculeInputs--====--//
  void calculeInputs() {
    inputs[0]=castRayLeft();
    inputs[1]=castRayCenter();
    inputs[2]=castRayRight();
    inputs[3]=map(directionChangeRate, 0, MAXDIR, 0, 1);
    inputs[4]=map(speed, 0, MAXSPEED, 0, 1);
    inputs[5]=map(acceleration.mag(), 0, 3, 0, 1);
    inputs[6]=(outputs!=null) ? outputs[0] : 0;
    inputs[7]=(outputs!=null) ? outputs[1] : 0;
    inputs[8]=(outputs!=null) ? outputs[2] : 0;
    inputs[9]=(outputs!=null) ? outputs[3] : 0;
  }
 
  
  
  
  //--====--ChoseAction--====--//
  void choseAction(int decision) {

    switch(decision) {
    case 0:
      run(DC.copy().normalize());//avance
      break;
    case 1:
      //droite
      DC.rotate(directionChangeRate);
      run(DC.copy().normalize());
      break;
    case 2:
      //gauche
      DC.rotate(-directionChangeRate);
      run(DC.copy().normalize());
      break;
    case 3:
      //break
      acceleration.mult(0.5);
      break;
    }
  }

  //--====--Fitness--====--//
  void calculeFitness() {

    if (!dead) {
      if(inputs[1]==-1){
     fitness -= inputs[1]*(PVector.sub(position, start)).mag()*0.01;
      }
    }
    if (position.x<0) {
      dead=true;
    }
    if (position.x>width) {
      dead=true;
    }
    if (position.y<0) {
      dead=true;
    }
    if (position.y>height) {
      dead=true;
    }

    if (inputR==0) {
      dead=true;
    }
    if (inputL==0) {
      dead=true;
    }
    if (inputC==0) {
      dead=true;
    }
  }


  //--====--CastRay--====--//
  float castRayCenter() {
    FRaycastResult resultC = new FRaycastResult();
    FBody c = world.raycastOne(position.x+DC.x, position.y+DC.y, directionC.x+DC.x, directionC.y+DC.y, resultC, true);
    if (c != null) {
      inputC = resultC.getLambda();
      fill(col);
      ellipse(resultC.getX(), resultC.getY(), 5, 5);
    } else {
      inputC = -1;
    }

    return inputC;
  }
  float castRayLeft() {

    FRaycastResult resultL = new FRaycastResult();
    FBody l = world.raycastOne(position.x+DL.x, position.y+DL.y, directionL.x+DL.x, directionL.y+DL.y, resultL, true);
    if (l != null) {
      inputL = resultL.getLambda();
      fill(col);
      ellipse(resultL.getX(), resultL.getY(), 5, 5);
    } else {
      inputL = -1;
    }

    return inputL;
  }
  float castRayRight() {

    FRaycastResult resultR = new FRaycastResult();
    FBody r = world.raycastOne(position.x+DR.x, position.y+DR.y, directionR.x+DR.x, directionR.y+DR.y, resultR, true);
    if (r != null) {
      inputR = resultR.getLambda();
      fill(col);
      ellipse(resultR.getX(), resultR.getY(), 5, 5);
    } else {
      inputR = -1;
    }

    return inputR;
  }
}





static class Name {
  static String[] names ={"adamant", "adroit", "amatory", "animistic", "antic", "arcadian", "baleful", "bellicose", "bilious", "boorish", "calamitous", "caustic", "cerulean", "comely", "concomitant", "contumacious", "corpulent", "crapulous", "defamatory", "didactic", "dilatory", "dowdy", "efficacious", "effulgent", "egregious", "endemic", "equanimous", "execrable", "fastidious", "feckless", "fecund", "friable", "fulsome", "garrulous", "guileless", "gustatory", "heuristic", "histrionic", "hubristic", "incendiary", "insidious", "insolent", "intransigent", "inveterate", "invidious", "irksome", "jejune", "jocular", "judicious", "lachrymose", "limpid", "loquacious", "luminous", "mannered", "mendacious", "meretricious", "minatory", "mordant", "munificent", "nefarious", "noxious", "obtuse", "parsimonious", "pendulous", "pernicious", "pervasive", "petulant", "platitudinous", "precipitate", "propitious", "puckish", "querulous", "quiescent", "rebarbative", "recalcitant", "redolent", "rhadamanthine", "risible", "ruminative", "sagacious", "salubrious", "sartorial", "sclerotic", "serpentine", "spasmodic", "strident", "taciturn", "tenacious", "tremulous", "trenchant", "turbulent", "turgid", "ubiquitous", "uxorious", "verdant", "voluble", "voracious", "wheedling", "withering", "zealous", "ninja", "chair", "pancake", "statue", "unicorn", "rainbows", "laser", "senor", "bunny", "captain", "nibblets", "cupcake", "carrot", "gnomes", "glitter", "potato", "salad", "toejam", "curtains", "beets", "toilet", "exorcism", "stick figures", "mermaid eggs", "sea barnacles", "dragons", "jellybeans", "snakes", "dolls", "bushes", "cookies", "apples", "ice cream", "ukulele", "kazoo", "banjo", "opera singer", "circus", "trampoline", "carousel", "carnival", "locomotive", "hot air balloon", "praying mantis", "animator", "artisan", "artist", "colorist", "inker", "coppersmith", "director", "designer", "flatter", "stylist", "leadman", "limner", "make-up artist", "model", "musician", "penciller", "producer", "scenographer", "set decorator", "silversmith", "teacher", "auto mechanic", "beader", "bobbin boy", "clerk of the chapel", "filling station attendant", "foreman", "maintenance engineering", "mechanic", "miller", "moldmaker", "panel beater", "patternmaker", "plant operator", "plumber", "sawfiler", "shop foreman", "soaper", "stationary engineer", "wheelwright", "woodworkers"};
  public static int max=names.length;
  static String give(int number) {
    return names[number];
  }
}
