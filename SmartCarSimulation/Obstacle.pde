class Obstacle {
  FCircle obstacle;

  Obstacle(float x, float y, float size) {
    obstacle = new FCircle(size);
    obstacle.setPosition(x, y);
    obstacle.setStatic(true);
    obstacle.setFill(255,255,50,25);
    obstacle.setRestitution(0);
    obstacle.setNoStroke();
    world.add(obstacle);
  }

}
