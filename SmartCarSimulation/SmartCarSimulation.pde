import java.util.*;
import java.lang.*;
import java.io.*;
import fisica.*;
import org.jbox2d.common.*;

color obs = color(255, 0, 0);

FWorld world;

ArrayList<NeuralNetwork> testTab = new ArrayList<NeuralNetwork>();
int counter =0;
PVector start;
final float radius = 15;

ArrayList<Car> creatures;
ArrayList<Obstacle> obstacles;

PImage car;
PImage backGround;
Tableau score;
int temps=0;
int maxTime=500;

boolean cacher=false;

void setup() {
  imageMode(CENTER);
  fullScreen();
  start = new PVector(30, height/2);
  smooth();
  backGround= loadImage("background.jpg");
  backGround.resize(width, height);
  car = loadImage("car.png");
  car.resize(car.width/60, car.height/60);
  Fisica.init(this);

  world = new FWorld();
  world.setEdges();
  world.setGravity(new Vec2(0, 0));


  creatures = new ArrayList<Car>();
  obstacles = new ArrayList<Obstacle>();

  for (int i=0; i<75; i++) {
    creatures.add(new Car(start.copy()));
  }






  score = new Tableau(creatures, width-300, 15);
}

void draw() {
  background(255);
  image(backGround,width/2,height/2);
  world.draw();
  world.step();
  noStroke();
  fill(255, 0, 0, 50);
  ellipse(start.x, start.y, radius*2, radius*2);

  for (Car c : creatures) {
    if (!c.dead)
      c.life();
  }
  Collections.sort(creatures, new SortFitness());
  if(cacher){
    score.show();
  }
  textSize(24);
  fill(0);
  text(temps+"/"+maxTime, width/2-20, 30);
  reset();
}

void reset() {
  temps++;
  if (temps>maxTime) {
    temps=0;
    ArrayList<Car> send = new ArrayList<Car>();
    for (Car c : creatures) {
      send.add(c);
    }
    creatures.clear();
    creatures=Training.train(send);
    score = new Tableau(creatures, width-300, 15);
  }
}

void keyPressed() {
  if (key=='s')
    maxTime=mouseX;
  if (key=='t')
    cacher = !cacher;
}

void mouseDragged() {
  if (keyPressed==true&&key=='o')
    if (counter%3==0) {
      obstacles.add(new Obstacle(mouseX, mouseY, 25));
    }
  counter++;
}


class SortFitness implements Comparator<Car> {
  public int compare(Car b, Car a)
  {
    return (int)a.fitness - (int)b.fitness;
  }
}
