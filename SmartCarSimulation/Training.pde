static class Training {

  static ArrayList<Car> trainedCreature = new ArrayList<Car>();

  static public ArrayList<Car> train(ArrayList<Car> creatures) {
    int ellitisme = 1;
    int nombreParNiveau = creatures.size();
    int counter =0;
    for (int i = 1; i < nombreParNiveau; i++) {
      if (counter>ellitisme) {
        creatures.get(i-1).brain.mix(creatures.get(i).brain);
      }
      counter++;
      trainedCreature.add(creatures.get(i-1));
    }
    trainedCreature.get(0).brain.mix(creatures.get(nombreParNiveau-1).brain);
    trainedCreature.get(0).brain.mix(creatures.get(nombreParNiveau-1).brain);
    trainedCreature.add(creatures.get(nombreParNiveau-1));

    counter =0;
    for (Car c : trainedCreature) {
      if (counter>ellitisme) {
        c.brain.mutate(150);
      }
      if (counter>nombreParNiveau-10) {
        c.brain.initiate();
        trainedCreature.get(0).brain.mix(c.brain);
        trainedCreature.get(0).brain.mix(c.brain);
      }
      c.reset();
      counter++;
    }
    return trainedCreature;
  }
}
