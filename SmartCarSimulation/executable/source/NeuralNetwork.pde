class NeuralNetwork {

  ArrayList<double[][]> neuralNetwork;
  ArrayList<double[]> bias;

  NeuralNetwork(int ... args) {
    neuralNetwork = new ArrayList<double[][]>();
    bias = new ArrayList<double[]>();
    int counter = 0;
    int inputsSize = 0;
    int outputSize = 0;
    int temp=0;
    for (int arg : args) {
      if (counter == 0) {
        inputsSize=arg;
      } else {
        outputSize=arg;
        if (temp==0) {
          neuralNetwork.add(new double[inputsSize][outputSize]);
        } else {
          neuralNetwork.add(new double[temp][outputSize]);
        }
        bias.add(new double[outputSize]);
        temp=arg;
      }
      counter++;
    }

    initiate();
  }




  void initiate() {
    for (double[][] a : neuralNetwork) {
      for (int i = 0; i<a.length; i++) {
        for (int j = 0; j<a[0].length; j++) {
          a[i][j]=random(-1, 1);
        }
      }
    }
    for (double[] a : bias) {
      for (int i = 0; i<a.length; i++) {
        a[i]=(int)random(-1, 1);
      }
    }
  }



  double[] fire(double[] inputs) {
    for (int i = 0; i < neuralNetwork.size(); i++) {
      double[] newInputs = new double[neuralNetwork.get(i)[0].length];
      for (int col = 0; col<neuralNetwork.get(i)[0].length; col++) {
        double newInput=0;
        for (int row = 0; row<neuralNetwork.get(i).length; row++) {
          newInput+=neuralNetwork.get(i)[row][col]*inputs[row];
        }

        newInput+=bias.get(i)[col];
        newInput=sig(newInput);
        newInputs[col]=newInput;
      }
      inputs=newInputs;
    }
    return inputs;
  }


  public double sig(double x) {
    return (1/( 1 + Math.pow(Math.E, (-1*x))));
  }


  public void mix(NeuralNetwork partenaire) {
    for (int x = 0; x < neuralNetwork.size(); x++) {
      for (int i = 0; i<neuralNetwork.get(x).length; i++) {
        for (int j = 0; j<neuralNetwork.get(x)[0].length; j++) {
          if (random(2)>1) {
            partenaire.neuralNetwork.get(x)[i][j]=neuralNetwork.get(x)[i][j];
          }
        }
      }
    }
    for (int x = 0; x < bias.size(); x++) {
      for (int i = 0; i<bias.get(x).length; i++) {
        if (random(2)>1) {
          partenaire.bias.get(x)[i]=bias.get(x)[i];
        }
      }
    }
  }


  public void mutate(int value) {
    for (int x = 0; x < neuralNetwork.size(); x++) {
      for (int i = 0; i<neuralNetwork.get(x).length; i++) {
        for (int j = 0; j<neuralNetwork.get(x)[0].length; j++) {
          if ((int)random(value)==1) {
            neuralNetwork.get(x)[i][j]=random(-1, 1);
          }
        }
      }
    }
    for (int x = 0; x < bias.size(); x++) {
      for (int i = 0; i<bias.get(x).length; i++) {
        if ((int)random(value)==1) {
          bias.get(x)[i]=(int)random(-1, 1);
        }
      }
    }
  }






  void show() {
    for (int n = 0; n < neuralNetwork.size(); n++) {
      for (int i = 0; i<neuralNetwork.get(n).length; i++) {
        for (int j = 0; j<neuralNetwork.get(n)[0].length; j++) {
          double size = neuralNetwork.get(n)[i][j];
          float dist = n*300;
          float x = (i*50)+250;
          float y = (j*50)+40;
          fill(255);
          noStroke();
          ellipse(x+dist, y, (float)size*75, (float)size*75);
          fill(0);
          textSize(0);
          text(((float)size), x+dist, y);
        }
      }
    }
  }
}
