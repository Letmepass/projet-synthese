import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.*; 
import java.lang.*; 
import java.io.*; 
import fisica.*; 
import org.jbox2d.common.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class SmartCarSimulation extends PApplet {







int obs = color(255, 0, 0);

FWorld world;

ArrayList<NeuralNetwork> testTab = new ArrayList<NeuralNetwork>();
int counter =0;
PVector start;
final float radius = 15;

ArrayList<Car> creatures;
ArrayList<Obstacle> obstacles;

PImage car;
PImage backGround;
Tableau score;
int temps=0;
int maxTime=500;

boolean cacher=false;

public void setup() {
  imageMode(CENTER);
  
  start = new PVector(30, height/2);
  
  backGround= loadImage("background.jpg");
  backGround.resize(width, height);
  car = loadImage("car.png");
  car.resize(car.width/60, car.height/60);
  Fisica.init(this);

  world = new FWorld();
  world.setEdges();
  world.setGravity(new Vec2(0, 0));


  creatures = new ArrayList<Car>();
  obstacles = new ArrayList<Obstacle>();

  for (int i=0; i<75; i++) {
    creatures.add(new Car(start.copy()));
  }






  score = new Tableau(creatures, width-300, 15);
}

public void draw() {
  background(255);
  image(backGround,width/2,height/2);
  world.draw();
  world.step();
  noStroke();
  fill(255, 0, 0, 50);
  ellipse(start.x, start.y, radius*2, radius*2);

  for (Car c : creatures) {
    if (!c.dead)
      c.life();
  }
  Collections.sort(creatures, new SortFitness());
  if(cacher){
    score.show();
  }
  textSize(24);
  fill(0);
  text(temps+"/"+maxTime, width/2-20, 30);
  reset();
}

public void reset() {
  temps++;
  if (temps>maxTime) {
    temps=0;
    ArrayList<Car> send = new ArrayList<Car>();
    for (Car c : creatures) {
      send.add(c);
    }
    creatures.clear();
    creatures=Training.train(send);
    score = new Tableau(creatures, width-300, 15);
  }
}

public void keyPressed() {
  if (key=='s')
    maxTime=mouseX;
  if (key=='t')
    cacher = !cacher;
}

public void mouseDragged() {
  if (keyPressed==true&&key=='o')
    if (counter%3==0) {
      obstacles.add(new Obstacle(mouseX, mouseY, 25));
    }
  counter++;
}


class SortFitness implements Comparator<Car> {
  public int compare(Car b, Car a)
  {
    return (int)a.fitness - (int)b.fitness;
  }
}
class Car {

  //--====--TRAIT--====--//

  final String[] inputText = {
    "Vision Gauche:", 
    "Vision Centre:", 
    "Vision Droite:", 
    "Angle de Rotation:", 
    "Vitesse:", 
    "Acceleration:", 
    "Avance:", 
    "Droite:", 
    "Gauche:", 
    "Break:"
  };
  final static float MAXSPEED = 0.2f;
  final static float MAXDIR = 0.04f;
  PVector start;
  //Nombre de inputs//
  final int nombreInputs = 10;
  //Nombre de inputs//0
  final int nombreOutputs = 6;
  NeuralNetwork brain;

  float fitness=0;

  String name;

  int col;
  PVector position;
  PVector acceleration;
  int size = 5;
  float vision = 10;
  float eyeDeviation=0.25f*PI;
  float directionChangeRate=0;
  float speed=0;
  double[] inputs = new double[nombreInputs];
  double[] outputs=null;
  boolean dead = false;
  //--====--Directions--====--//

  PVector directionL;
  PVector directionC;
  PVector directionR;

  PVector DL;
  PVector DC;
  PVector DR;

  float inputR=1;
  float inputL=1;
  float inputC=1;


  //--====--Constructeur--====--// 
  Car(PVector start) {
  
    this.start=start;
    outputs=null;
    acceleration= new PVector();
    col = color(random(255), random(255), random(255));
    name = Name.give(PApplet.parseInt(random(Name.max)));
    brain = new NeuralNetwork(nombreInputs, nombreOutputs);
    position = start.copy();
    DC = PVector.random2D();
    DC = new PVector(1, 0);
  }
  //--====--Reset--====--//
  public void reset() {
    speed=0;
    directionChangeRate=0;
    fitness=0;
    acceleration= new PVector();
    position = start.copy();
    DC = PVector.random2D();
    DC = new PVector(1, 0);

    dead = false;
    update();
    inputR=1;
    inputL=1;
    inputC=1;
  }
  //--====--Update--====--//
  public void update() {
    DC.setMag(size+5);
    DL = DC.copy();
    DR = DC.copy();
    DL.rotate(eyeDeviation); 
    DR.rotate(-eyeDeviation);
    directionR = PVector.add(DR.copy().mult(vision), position);
    directionL = PVector.add(DL.copy().mult(vision), position);
    directionC = PVector.add(DC.copy().mult(vision), position);
  }
  //--====--Show--====--//
  public void show() {
    if (!dead&&(PVector.sub(position, start)).mag()>radius) {
      pushMatrix();
      translate(position.x, position.y); 
      rotate(DC.copy().heading()+PI/2); 
      image(car, 0, 0);
      popMatrix();
      fill(col);
      ellipse(position.x, position.y, size, size);
      rectMode(CENTER);
      textSize(25);
      if (PVector.sub(position, new PVector(mouseX, mouseY)).mag()<15) {
        text(name, position.x, position.y-150);
        text("Fitness : "+fitness, position.x, position.y-100);
        textSize(15);
        fill(50, 50, 50, 20);
        strokeWeight(5);
        stroke(col);
        rect(position.x, position.y, 50, 50);
        strokeWeight(1);
        fill(col);
        for (int i =0; i<nombreInputs; i++) {
          text(inputText[i]+" "+((float)inputs[i])+"", position.x+50, position.y+i*15-50);
        }
        noStroke();
        if (keyPressed) {
          if (key=='d') {
            fitness-=1000;
          }
          if (key=='a') {
            fitness+=1000;
          }
        }
      }
      rectMode(0);
    } else {
      pushMatrix();
      translate(position.x, position.y); 
      rotate(DC.copy().heading()+PI/2); 
      tint(255, 5);
      image(car, 0, 0);
      tint(255, 255);
      popMatrix();
      fill(col, 5);
      ellipse(position.x, position.y, size, size);
    }
  }
  //--====--Run--====--//
  public void run(PVector direction) {
    acceleration.add(direction.mult(speed));
    position.add(acceleration);
  }

  //--====--Life--====--//
  public void life() {
    update();
    show();
    calculeInputs();
    outputs = brain.fire(inputs);
    int decision=0;
    double max=0;
    for (int i=0; i<4; i++) {
      if (outputs[i]>max) {
        decision=i;
        max=outputs[i];
      }
    }
    outputs[0]=0;
    outputs[1]=0;
    outputs[2]=0;
    outputs[3]=0;
    outputs[decision]=1;
    directionChangeRate=(float)(outputs[4]*MAXDIR*PI);
    speed=(float)(outputs[5]*MAXSPEED);
    choseAction(decision);
    calculeFitness();
  }
  //--====--calculeInputs--====--//
  public void calculeInputs() {
    inputs[0]=castRayLeft();
    inputs[1]=castRayCenter();
    inputs[2]=castRayRight();
    inputs[3]=map(directionChangeRate, 0, MAXDIR, 0, 1);
    inputs[4]=map(speed, 0, MAXSPEED, 0, 1);
    inputs[5]=map(acceleration.mag(), 0, 3, 0, 1);
    inputs[6]=(outputs!=null) ? outputs[0] : 0;
    inputs[7]=(outputs!=null) ? outputs[1] : 0;
    inputs[8]=(outputs!=null) ? outputs[2] : 0;
    inputs[9]=(outputs!=null) ? outputs[3] : 0;
  }
 
  
  
  
  //--====--ChoseAction--====--//
  public void choseAction(int decision) {

    switch(decision) {
    case 0:
      run(DC.copy().normalize());//avance
      break;
    case 1:
      //droite
      DC.rotate(directionChangeRate);
      run(DC.copy().normalize());
      break;
    case 2:
      //gauche
      DC.rotate(-directionChangeRate);
      run(DC.copy().normalize());
      break;
    case 3:
      //break
      acceleration.mult(0.5f);
      break;
    }
  }

  //--====--Fitness--====--//
  public void calculeFitness() {

    if (!dead) {
      if(inputs[1]==-1){
     fitness -= inputs[1]*(PVector.sub(position, start)).mag()*0.01f;
      }
    }
    if (position.x<0) {
      dead=true;
    }
    if (position.x>width) {
      dead=true;
    }
    if (position.y<0) {
      dead=true;
    }
    if (position.y>height) {
      dead=true;
    }

    if (inputR==0) {
      dead=true;
    }
    if (inputL==0) {
      dead=true;
    }
    if (inputC==0) {
      dead=true;
    }
  }


  //--====--CastRay--====--//
  public float castRayCenter() {
    FRaycastResult resultC = new FRaycastResult();
    FBody c = world.raycastOne(position.x+DC.x, position.y+DC.y, directionC.x+DC.x, directionC.y+DC.y, resultC, true);
    if (c != null) {
      inputC = resultC.getLambda();
      fill(col);
      ellipse(resultC.getX(), resultC.getY(), 5, 5);
    } else {
      inputC = -1;
    }

    return inputC;
  }
  public float castRayLeft() {

    FRaycastResult resultL = new FRaycastResult();
    FBody l = world.raycastOne(position.x+DL.x, position.y+DL.y, directionL.x+DL.x, directionL.y+DL.y, resultL, true);
    if (l != null) {
      inputL = resultL.getLambda();
      fill(col);
      ellipse(resultL.getX(), resultL.getY(), 5, 5);
    } else {
      inputL = -1;
    }

    return inputL;
  }
  public float castRayRight() {

    FRaycastResult resultR = new FRaycastResult();
    FBody r = world.raycastOne(position.x+DR.x, position.y+DR.y, directionR.x+DR.x, directionR.y+DR.y, resultR, true);
    if (r != null) {
      inputR = resultR.getLambda();
      fill(col);
      ellipse(resultR.getX(), resultR.getY(), 5, 5);
    } else {
      inputR = -1;
    }

    return inputR;
  }
}





static class Name {
  static String[] names ={"adamant", "adroit", "amatory", "animistic", "antic", "arcadian", "baleful", "bellicose", "bilious", "boorish", "calamitous", "caustic", "cerulean", "comely", "concomitant", "contumacious", "corpulent", "crapulous", "defamatory", "didactic", "dilatory", "dowdy", "efficacious", "effulgent", "egregious", "endemic", "equanimous", "execrable", "fastidious", "feckless", "fecund", "friable", "fulsome", "garrulous", "guileless", "gustatory", "heuristic", "histrionic", "hubristic", "incendiary", "insidious", "insolent", "intransigent", "inveterate", "invidious", "irksome", "jejune", "jocular", "judicious", "lachrymose", "limpid", "loquacious", "luminous", "mannered", "mendacious", "meretricious", "minatory", "mordant", "munificent", "nefarious", "noxious", "obtuse", "parsimonious", "pendulous", "pernicious", "pervasive", "petulant", "platitudinous", "precipitate", "propitious", "puckish", "querulous", "quiescent", "rebarbative", "recalcitant", "redolent", "rhadamanthine", "risible", "ruminative", "sagacious", "salubrious", "sartorial", "sclerotic", "serpentine", "spasmodic", "strident", "taciturn", "tenacious", "tremulous", "trenchant", "turbulent", "turgid", "ubiquitous", "uxorious", "verdant", "voluble", "voracious", "wheedling", "withering", "zealous", "ninja", "chair", "pancake", "statue", "unicorn", "rainbows", "laser", "senor", "bunny", "captain", "nibblets", "cupcake", "carrot", "gnomes", "glitter", "potato", "salad", "toejam", "curtains", "beets", "toilet", "exorcism", "stick figures", "mermaid eggs", "sea barnacles", "dragons", "jellybeans", "snakes", "dolls", "bushes", "cookies", "apples", "ice cream", "ukulele", "kazoo", "banjo", "opera singer", "circus", "trampoline", "carousel", "carnival", "locomotive", "hot air balloon", "praying mantis", "animator", "artisan", "artist", "colorist", "inker", "coppersmith", "director", "designer", "flatter", "stylist", "leadman", "limner", "make-up artist", "model", "musician", "penciller", "producer", "scenographer", "set decorator", "silversmith", "teacher", "auto mechanic", "beader", "bobbin boy", "clerk of the chapel", "filling station attendant", "foreman", "maintenance engineering", "mechanic", "miller", "moldmaker", "panel beater", "patternmaker", "plant operator", "plumber", "sawfiler", "shop foreman", "soaper", "stationary engineer", "wheelwright", "woodworkers"};
  public static int max=names.length;
  public static String give(int number) {
    return names[number];
  }
}
class NeuralNetwork {

  ArrayList<double[][]> neuralNetwork;
  ArrayList<double[]> bias;

  NeuralNetwork(int ... args) {
    neuralNetwork = new ArrayList<double[][]>();
    bias = new ArrayList<double[]>();
    int counter = 0;
    int inputsSize = 0;
    int outputSize = 0;
    int temp=0;
    for (int arg : args) {
      if (counter == 0) {
        inputsSize=arg;
      } else {
        outputSize=arg;
        if (temp==0) {
          neuralNetwork.add(new double[inputsSize][outputSize]);
        } else {
          neuralNetwork.add(new double[temp][outputSize]);
        }
        bias.add(new double[outputSize]);
        temp=arg;
      }
      counter++;
    }

    initiate();
  }




  public void initiate() {
    for (double[][] a : neuralNetwork) {
      for (int i = 0; i<a.length; i++) {
        for (int j = 0; j<a[0].length; j++) {
          a[i][j]=random(-1, 1);
        }
      }
    }
    for (double[] a : bias) {
      for (int i = 0; i<a.length; i++) {
        a[i]=(int)random(-1, 1);
      }
    }
  }



  public double[] fire(double[] inputs) {
    for (int i = 0; i < neuralNetwork.size(); i++) {
      double[] newInputs = new double[neuralNetwork.get(i)[0].length];
      for (int col = 0; col<neuralNetwork.get(i)[0].length; col++) {
        double newInput=0;
        for (int row = 0; row<neuralNetwork.get(i).length; row++) {
          newInput+=neuralNetwork.get(i)[row][col]*inputs[row];
        }

        newInput+=bias.get(i)[col];
        newInput=sig(newInput);
        newInputs[col]=newInput;
      }
      inputs=newInputs;
    }
    return inputs;
  }


  public double sig(double x) {
    return (1/( 1 + Math.pow(Math.E, (-1*x))));
  }


  public void mix(NeuralNetwork partenaire) {
    for (int x = 0; x < neuralNetwork.size(); x++) {
      for (int i = 0; i<neuralNetwork.get(x).length; i++) {
        for (int j = 0; j<neuralNetwork.get(x)[0].length; j++) {
          if (random(2)>1) {
            partenaire.neuralNetwork.get(x)[i][j]=neuralNetwork.get(x)[i][j];
          }
        }
      }
    }
    for (int x = 0; x < bias.size(); x++) {
      for (int i = 0; i<bias.get(x).length; i++) {
        if (random(2)>1) {
          partenaire.bias.get(x)[i]=bias.get(x)[i];
        }
      }
    }
  }


  public void mutate(int value) {
    for (int x = 0; x < neuralNetwork.size(); x++) {
      for (int i = 0; i<neuralNetwork.get(x).length; i++) {
        for (int j = 0; j<neuralNetwork.get(x)[0].length; j++) {
          if ((int)random(value)==1) {
            neuralNetwork.get(x)[i][j]=random(-1, 1);
          }
        }
      }
    }
    for (int x = 0; x < bias.size(); x++) {
      for (int i = 0; i<bias.get(x).length; i++) {
        if ((int)random(value)==1) {
          bias.get(x)[i]=(int)random(-1, 1);
        }
      }
    }
  }






  public void show() {
    for (int n = 0; n < neuralNetwork.size(); n++) {
      for (int i = 0; i<neuralNetwork.get(n).length; i++) {
        for (int j = 0; j<neuralNetwork.get(n)[0].length; j++) {
          double size = neuralNetwork.get(n)[i][j];
          float dist = n*300;
          float x = (i*50)+250;
          float y = (j*50)+40;
          fill(255);
          noStroke();
          ellipse(x+dist, y, (float)size*75, (float)size*75);
          fill(0);
          textSize(0);
          text(((float)size), x+dist, y);
        }
      }
    }
  }
}
class Obstacle {
  FCircle obstacle;

  Obstacle(float x, float y, float size) {
    obstacle = new FCircle(size);
    obstacle.setPosition(x, y);
    obstacle.setStatic(true);
    obstacle.setFill(255,255,50,25);
    obstacle.setRestitution(0);
    obstacle.setNoStroke();
    world.add(obstacle);
  }

}
class Tableau {
  ArrayList<Car> creature;
  int positionX;
  int positionY;
  int largeur=138;
  int hauteur=20;

  Tableau(ArrayList<Car> creature, int positionX, int positionY) {
    this.creature=creature;
    this.positionX=positionX;
    this.positionY=positionY;
  }


  public void show() {

    textSize(15);
    int x=positionX;
    int y=positionY+30;
    textSize(10);
    for (Car c : creature) {
      fill(c.col, 160);
      stroke(1, 255);
      rect(x, y, largeur, hauteur);
      rect(x+largeur, y, largeur/4, hauteur);
      ellipse(x+5, y+hauteur/2, 5, 5);
      fill(0);
      text(c.name, x+10, y+15);
      text(round(c.fitness), x+5+largeur, y+15);
      y+=hauteur;
    }
  }
}
static class Training {

  static ArrayList<Car> trainedCreature = new ArrayList<Car>();

  static public ArrayList<Car> train(ArrayList<Car> creatures) {
    int ellitisme = 1;
    int nombreParNiveau = creatures.size();
    int counter =0;
    for (int i = 1; i < nombreParNiveau; i++) {
      if (counter>ellitisme) {
        creatures.get(i-1).brain.mix(creatures.get(i).brain);
      }
      counter++;
      trainedCreature.add(creatures.get(i-1));
    }
    trainedCreature.get(0).brain.mix(creatures.get(nombreParNiveau-1).brain);
    trainedCreature.get(0).brain.mix(creatures.get(nombreParNiveau-1).brain);
    trainedCreature.add(creatures.get(nombreParNiveau-1));

    counter =0;
    for (Car c : trainedCreature) {
      if (counter>ellitisme) {
        c.brain.mutate(150);
      }
      if (counter>nombreParNiveau-10) {
        c.brain.initiate();
        trainedCreature.get(0).brain.mix(c.brain);
        trainedCreature.get(0).brain.mix(c.brain);
      }
      c.reset();
      counter++;
    }
    return trainedCreature;
  }
}
  public void settings() {  fullScreen();  smooth(); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "SmartCarSimulation" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
