class Tableau {
  ArrayList<Car> creature;
  int positionX;
  int positionY;
  int largeur=138;
  int hauteur=20;

  Tableau(ArrayList<Car> creature, int positionX, int positionY) {
    this.creature=creature;
    this.positionX=positionX;
    this.positionY=positionY;
  }


  void show() {

    textSize(15);
    int x=positionX;
    int y=positionY+30;
    textSize(10);
    for (Car c : creature) {
      fill(c.col, 160);
      stroke(1, 255);
      rect(x, y, largeur, hauteur);
      rect(x+largeur, y, largeur/4, hauteur);
      ellipse(x+5, y+hauteur/2, 5, 5);
      fill(0);
      text(c.name, x+10, y+15);
      text(round(c.fitness), x+5+largeur, y+15);
      y+=hauteur;
    }
  }
}
